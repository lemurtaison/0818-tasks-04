//
//  main.c
//  Task04-1
//
//  Created by Blincov Sergey on 30.05.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//
#include <stdio.h>

int kollats(unsigned int i);

int main(){
	unsigned int coun = 0, max = 0,t = 0;
	for (unsigned int i = 2; i <= 1000000; i++){
		coun = kollats(i);
		if (coun > max){
			max = coun;
			t = i;
		}
		
	}
	printf("kollats = %i, number = %i\n", max,t);
	return 0;
}

int kollats(unsigned int i){
	unsigned int coun = 0;
	for (;;){
		if (i == 1){
			break;
		}
		if (i % 2 == 0){
			i = i / 2;
			coun++;
		}
		else if (i % 2 == 1){
			i = 3*i + 1;
			coun++;
		}
	}
	return coun;
}
