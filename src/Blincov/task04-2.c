//
//  main.c
//  task04-2
//
//  Created by Blincov Sergey on 30.05.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define N 128
#define M 20

int splitting_string(char arr[M][N], char str[N]){
	int i, tmp, count;
	tmp = 0; count = 0;
	for(i = 0; i < strlen(str)-1; i++)
	{
		if((str[i] != ' ') && (str[i+1] != ' '))
		{
			arr[count][tmp] = str[i];
			tmp++;
		} else if((str[i] != ' ') && ((str[i+1] == ' ') || (str[i+1] == '\n')))
		{
			arr[count][tmp] = str[i];
			arr[count][tmp+1] = '\0';
			count++;
			tmp = 0;
		}
	}
	return count+1;
}

void index_sort(int count, int index[M])
{
	int i, tmp, rnd;
	srand(time(0));
	
	for(i = 0; i < count; i++)
	{
		index[i] = i;
	}
	
	for(i = 0; i < count; i++)
	{
		rnd = rand() % count;
		tmp = index[i];
		index[i] = index[rnd];
		index[rnd] = tmp;
	}
}

int main()
{
	char str[N];
	char words[M][N];
	FILE *i_file, *o_file;
	int index[M], count, i;
	
	i_file = fopen("input.txt", "rt");
	o_file = fopen("output.txt","wt");
	
	if(i_file != 0)
	{
		while(fgets(str, sizeof(str), i_file))
		{
			count = splitting_string(words, str);
			index_sort(count, index);
			for(i = 0; i < count ; i++)
			{
				fputs(words[index[i]], o_file);
				fputc(' ', o_file);
			}
			fputc('\n', o_file);
		}
	}
	else
	{
		printf("Error\n");
	}
	fclose(i_file);
	fclose(o_file);
	return 0;
}