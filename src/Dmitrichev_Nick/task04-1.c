/*
������� ����� �� ���������� [0,MAX], ������� ��������� ����� ������� ������������������ ��������,
� ����� ���� ������������������
*/
#include <stdio.h>
#define MAX 1000000
int Collatz(unsigned long Cur_Num)
{
	int Res = 0;
	while (Cur_Num != 1)
	{
		if (Cur_Num % 2 == 0)
			Cur_Num = (__int64)(Cur_Num / 2);
		else
			Cur_Num = 3 * Cur_Num + 1;
		Res++;
	}
	return Res;
}
int main()
{
	int Count_Left = 2,Count_Right=MAX;
	int True_Count = 0, Tmp_Count = 0,Res;
	for (Count_Left;Count_Left < Count_Right;Count_Left++,Count_Right--)//����� ������������ � ������ � ����� ����������
	{
		Tmp_Count = Collatz(Count_Left);
		if (Tmp_Count > True_Count)
		{
			True_Count = Tmp_Count;
			Res = Count_Left;
		}
		Tmp_Count = Collatz(Count_Right);
		if (Tmp_Count > True_Count)
		{
			True_Count = Tmp_Count;
			Res = Count_Right;
		}
	}
	printf("Number: %d\nChain length: %d\n", Res,True_Count);
	return 0;
}