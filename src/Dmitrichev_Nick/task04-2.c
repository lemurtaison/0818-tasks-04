/*
��������� ������ ��������� ���� � ������������ �������� ����� � ������ ������
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#define Word_Sym 20 //����������� �� ����� �����
#define File_Str 20 //����������� �� ���������� �����
#define Str_Sym 255 //����������� �� ���������� �������� � ������
char Str_In_F(FILE *Str_F, char *Y_Name)//������� ���������� ����� � ����� 
{
	char i = 0;
	char buf[256];
	while (!feof(Str_F))
		if (fgets(buf, 256, Str_F))
			i++;
	fclose(Str_F);
	Str_F = fopen(Y_Name, "rt");
	return i;
}
void Swap(char *Str[], char i, char j)//������������ ��� ������
{
	char *tmp = Str[i];
	Str[i] = Str[j];
	Str[j] = tmp;
}
void Random(char *Str[], char Num_E)//������������ ����� � ������ � ��������� �������
{
	srand(time(NULL));
	char i = 0;
	for (i = 0;i < 2 * Num_E;i++)
		Swap(Str, rand() % Num_E, rand() % Num_E);
}
int Word_Str(char *Str)//������� ���������� ���� � ������
{
	char i = 0, Res = 0;
	while (Str[i++] != '\0')
		if (Str[i] == ' ')
			Res++;
	return Res + 1;
}
int main()
{
	FILE *Str_F;
	char Co1 = 0, Co2 = 0, Co3 = 0, Co4;//��������
	char True_Word_Str;
	char File_Name[9];

	puts("Enter the name of your file ('name.txt' for example)");//�������� ������� �����
	fgets(File_Name, 9, stdin);
	while ((Str_F = fopen(File_Name, "rt")) == 0)
	{
		puts("\nThe program didn't find this file\nTry again");
		fgets(File_Name, 9, stdin);
	}
	puts("\nThe file has been found");
	char Str_File = Str_In_F(Str_F, File_Name);
	char **buf = (char**)malloc(Str_File * sizeof(char*));//������ ��� ����� �� �����   
	for (Co2 = 0;Co2 < Str_File;++Co2)
		buf[Co2] = malloc(sizeof(char)*Word_Sym);


	char **Str = (char**)malloc(Word_Sym * sizeof(char*));//������ ��� ����� � ������
	for (Co2 = 0;Co2 < Word_Sym;++Co2)
		Str[Co2] = malloc(sizeof(char)*Word_Sym);


	while ((Co1 <Str_File) && (fgets(buf[Co1], Str_Sym, Str_F)))//���������� buf �������� �� �����   
	{
		Co2 = 0;
		while ((Co2 < strlen(buf[Co1]) + 1) && (buf[Co1][Co2] != '\n') && (buf[Co1][Co2] != '\0'))
			Co2++;
		buf[Co1][Co2] = '\0';
		Co1++;
	}

	fclose(Str_F);

	for (Co1 = 0;Co1 < Str_File;Co1++)
	{
		True_Word_Str = Word_Str(buf[Co1]);
		Co2 = 0;
		for (Co4 = 0;Co4 < True_Word_Str;Co4++)
		{
			while ((buf[Co1][Co2] != ' ') && (buf[Co1][Co2] != '\n') && (buf[Co1][Co2] != '\0'))
				Str[Co4][Co3++] = buf[Co1][Co2++];
			Str[Co4][Co3] = '\0';
			Co2++;
			Co3 = 0;
		}
		Random(Str, True_Word_Str);

		Co2 = 0;
		Co3 = 0;
		for (Co4 = 0;Co4 < True_Word_Str;Co4++)//������ � buf ����� ������������
		{
			while ((Str[Co4][Co3] != '\0') && (Str[Co4][Co3] != '\n') && (Str[Co4][Co3] != ' '))
				buf[Co1][Co2++] = Str[Co4][Co3++];
			buf[Co1][Co2++] = ' ';
			Co3 = 0;
		}
		buf[Co1][Co2 - 1] = '\0';
	}

	Str_F = fopen(File_Name, "wt");
	for (Co1 = 0;Co1 < Str_File;Co1++)//������ � ���� 
		fprintf(Str_F, "%s\n", buf[Co1]);
	fclose(Str_F);
	free(Str);
	free(buf);
	puts("\nThe program successfully finished work\n");
	return 0;
}