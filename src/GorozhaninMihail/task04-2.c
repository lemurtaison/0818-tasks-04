#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define N 256  
#define M 32 
void sort(int A,int *num)
{
	int s, rand_ind, tmp;
	srand(time(0));
	for (s = 0; s < A; s++)
	{
		rand_ind = rand() % A;
		tmp = num[s];
		num[s] = num[rand_ind];
		num[rand_ind] = tmp;
	}
}

void split(FILE *f_in,FILE *f_out)
{
	int *num,s;
	char str[M][N];
	char ***word;
	int count = 0, i = 0, j = 0, k, l;
	while (fgets(str[i], N, f_in))
		count++;
	rewind(f_in);
	word = (char***)malloc(count*sizeof(char**));
	while (fgets(str[i], N, f_in))
	{
		str[i][strlen(str[i]) - 1] = '\0';
		k = 0; l = 0;
		word[i] = (char**)malloc(strlen(str[i])*sizeof(char*));
		for (j = 0; j <= strlen(str[i]); j++)
			if (((str[i][j] <= '9') && (str[i][j] >= '0')) || ((str[i][j] <= 'Z') && (str[i][j] >= 'A')) || ((str[i][j] <= 'z') && (str[i][j] >= 'a')))
			{
				l++;
			}
			else
			{
				if (l != 0) 
				{
					word[i][k] = (char*)malloc(l*sizeof(char));
					k++;
				}
				l=0;
			}
		k = 0;
		l = 0;
		for (j = 0; j < strlen(str[i]); j++)
			if (((str[i][j] <= '9') && (str[i][j] >= '0')) || ((str[i][j] <= 'Z') && (str[i][j] >= 'A')) || ((str[i][j] <= 'z') && (str[i][j] >= 'a')))
			{
				word[i][k][l]=str[i][j];
				l++;
			}
			else
			{
				if (l != 0)
				{
					word[i][k][l] = '\0';
					k++;
				}
				l = 0;
			}
		num = (int*)malloc((k)*sizeof(int));
		for (s = 0; s < k; s++)
			num[s] = s;
		sort((k),num);
		for (int s = 0; s < k; s++)
		{
			fputs(word[i][num[s]], f_out);
			fputs(" ", f_out);
		}
		fputs("\n", f_out);
		i++;
		printf("String number %d recorded\n",i);
	}
	free(num);
	free(word);
}
int main()
{
	FILE *f_in, *f_out;
	char *fnamein = "D:\\input_data.txt", *fnameout= "D:\\output_data.txt";
	f_in = fopen(fnamein, "rt");
	f_out = fopen(fnameout, "wt");
	if (f_in == NULL) 
	{
		printf("File is not found.\n");
		exit(1);
	}
	split(f_in,f_out);
	fclose(f_in);
	fclose(f_out);
	return 0;
}