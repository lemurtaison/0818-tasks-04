#include <stdio.h>
#include <math.h>
#define UPBOARDR 1000000

void NewNum(unsigned long *p, int *c)
{	
	if(*p > 1)
	{
		
		if(*p % 2 == 0)
		{	
			*c = *c + 1;
			*p = *p / 2;
			NewNum(p, c);
		}
		if ((*p != 1)&&(*p % 2 == 1))
		{
			*c = *c + 1;
			*p = *p * 3 + 1;
			NewNum(p, c);
		}	
	}
}

int main()
{
	unsigned long i = 2, res = 0, buf;
	int  maxcount = 0, count;
	while(i<UPBOARDR)
	{
		buf = i; count = 0;
		NewNum(&buf, &count);
		if (count > maxcount)
		{
			maxcount = count;
			res = i;
		}
		i++;
	}
	printf("N = %d, Count = %d \n", res, maxcount);
} 