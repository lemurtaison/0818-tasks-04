#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 256

int lsepartr(char *buf, char lngth, char arrw[N][N])
{
	int i=0, j=0, k=0;

	buf[lngth-1] = ' ';
	while(i < lngth)
	{
		while(buf[i] != ' ') // ������ ���� ������� ����� ����-�� ��������, ����������� ��������
		{
			arrw[k][j] = buf[i];
			j++; i++;
		}
		if(j > 0)
		{
			arrw[k][j++] = ' ';
			arrw[k][j] = 0;
			k++; j = 0;
		}
		i++;
	}
	if (k == 1)
		arrw[0][strlen(arrw[0])-1] = '\n';
	return k;
}

void lgenertr(char *str, char arrw[N][N], int k)
{
	int flag[N / 2];
	int i, j, n;
	for(i = 0; i < k; i++)
		flag[i] = 0;
	i = j = 0;
	n = rand()%k;
	while(i < k)
	{
		while(flag[n] != 0)
			n = rand()%k;
		flag[n] = 1;
		strcat(str, arrw[n]);
		i++;
	}
	str[strlen(str)-1] = '\n';
}

int main()
{
	FILE *fin;
	FILE *fout;
	char str[N];
	char arrw[N][N];
	char result[N][N];
	int i = 0, j, numw;
	
	fin = fopen("strings.txt", "rt");
	srand(time(0));
	while(fgets(str, N, fin))
	{
		numw = lsepartr(str, strlen(str), arrw);
		if (numw != 1)
		{
			str[0] = 0;
			lgenertr(str, arrw, numw);
		}
		else
			strcpy(str, arrw[0]);
		arrw[0][0] = 0;
		strcpy(result[i++], str);
	}	
	fclose(fin);
	fout = fopen("result.txt", "wt");
	for (j = 0; j < i; j++)
		fprintf(fout, "%s", result[j]);
	fclose(fout);
	return 0;
}