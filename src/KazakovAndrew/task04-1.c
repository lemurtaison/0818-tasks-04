﻿// Написать программу, которая находит в диапазоне целых чисел от 2 до 1000000 число, формирующее самую длинную последовательность Коллатца
#include <stdio.h>
#include <locale.h>
#define LRANGE 2
#define RRANGE 1000000

int main() {

	int	i;
	int	seq_length,
		max_seq_length = 0;
	long long int	curr_num,
					max_seq_len_num;

	setlocale(LC_ALL, "rus");

	for (i = LRANGE; i <= RRANGE; i++) {
		
		curr_num = i;
		seq_length = 1;

		while (curr_num != 1) {
			curr_num = (curr_num % 2 == 0) ? curr_num / 2 : 3 * curr_num + 1;
			seq_length++;
		}

		if (seq_length > max_seq_length) {
			max_seq_length = seq_length;
			max_seq_len_num = i;
		}

	}

	printf("Число из промежутка от %d до %d, формирующее самую длинную последовательность Коллатца: %d\n", LRANGE, RRANGE, max_seq_len_num);

	return 0;
}