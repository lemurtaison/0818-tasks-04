﻿// Написать программу, которая читает построчно текстовый файл и переставляет случайно слова в каждой строке.
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <stdlib.h>
#include <time.h>
#define IN_FILENAME "input.txt"
#define OUT_FILENAME "output.txt"
#define MAX_STR_LENGTH 200 // Макс. кол-во символов, которое будет считано из каждой строки файла

int main() {

	FILE *in, *out;
	int i, j, k;
	unsigned int rand_i;
	int temp;
	char	str[MAX_STR_LENGTH],
			newstr[MAX_STR_LENGTH];
	unsigned int words_positions[MAX_STR_LENGTH];
	unsigned int words_amount;

	setlocale(LC_ALL, "rus");
	srand(time(0));

	in = fopen(IN_FILENAME, "rt");
	out = fopen(OUT_FILENAME, "wt");

	// Обнуляем массив
	for (i = 0; i < MAX_STR_LENGTH; i++) {
		words_positions[i] = 0;
	}

	if (in && out) {

		while (fgets(str, MAX_STR_LENGTH, in)) {

			words_amount = 0;
			if (str[strlen(str)-1] == '\n') {
				str[strlen(str)-1] = 0;
			}

			// 1. Ищем позиции слов
			for (i = 0; i < strlen(str); i++) {
				// Следующее условие определяет позицию нового слова 
				if (str[i] != ' ' && (i == 0 || str[i - 1] == ' ')) {
					words_positions[words_amount] = i;
					words_amount++;
				}
			}

			// 2. Меняем очерёдность слов (мешаем массив с позициями)
			for (i = 0; i < words_amount; i++) {
				rand_i = rand() % words_amount;
				temp = words_positions[rand_i];
				words_positions[rand_i] = words_positions[i];
				words_positions[i] = temp;
			}
			
			// 3. Формируем перемешанную строку
			k = 0;
			for (i = 0; i < words_amount; i++) {
				j = words_positions[i];

				while (str[j] != ' ' && str[j] != 0) {
					newstr[k] = str[j];
					j++;
					k++;
				}
				newstr[k] = ' ';
				k++;
			}

			newstr[k - 1] = '\n';
			newstr[k] = 0;

			fputs(newstr, out);

		} // Конец while: пока строка не закончилась

		printf("Работа программы завершена. Проверьте %s.\n", OUT_FILENAME);

		fclose(in);
		fclose(out);

	} else {
		
		printf("При открытии одного из файлов, %s или %s, произошла ошибка:\n", IN_FILENAME, OUT_FILENAME);
		perror("");
		
		exit(1);
		
	}

	return 0;
}