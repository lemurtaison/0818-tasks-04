#define N 1000000
#include <stdio.h>

void sequence(int n)
{
	if (n%2==0)
		{
			n=n/2;
		}
		else
		{
			n=n*3+1;
		}
	if (n>1) {sequence(n);}
	printf("%i ",n);
}

int process (int i)
{
	int c;
	while (i>1)
	{
		if (i%2==0)
		{
			i=i/2;
			c++;
		}
		else
		{
			i=i*3+1;
			c++;
		}
	}
	return c;
}

int main ()
{
	int n,k,max,ourN;
	max=0;
	for (n=N;n>N/2;n--);
	{
		k=process(n);
		if (k>max)
		{
			max=k;
			ourN=n;
		}
	}
	sequence(ourN);
	return 0;
}