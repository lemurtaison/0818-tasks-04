#define N 256
#include <stdio.h>
#include <string.h>
#include <cstdlib>

void StringPrint(char* arr)
{
	int i=0;
	while ((arr[i])!='\0')
	{
		printf("%c",arr[i]);
		i++;
	}
}

void swap (int seat1 , int seat2)
{
	int k;				
	k=seat1;
	seat1=seat2;
	seat2=k;
}


void randStr(int *seat,int n)
{
	int i;
	for (i=0;i<n/2;i++)
		swap(seat[rand()%n],seat[rand()%n]);
}

void change(char* arr)
{
	char** mstr;
	int i=0;
	int j=0;
	int* numb;
	mstr=(char**)malloc(1*sizeof(char));
	mstr[0]=(char*)malloc(N*sizeof(char));
	while((arr[i]!='\0'))
	{
		mstr[i][j]=arr[i];
		if (arr[i]==' ')
		{
			j=0;
			i++;
			mstr[i]=(char*)malloc(N*sizeof(char));
		}
	}
	numb=(int*)malloc(i*sizeof(int));
	for (j=0;j<i;j++)
		numb[j]=j;
	randStr(numb,i);
	for (j=0;j<i;j++)
		StringPrint(mstr[j]);
}

int main()
{
	int i,n;
	char** arr;
	printf("How many strings you entered?\n");
	scanf("%i",&n);
	arr=(char**)malloc(n*sizeof(char*));
	for (i=0;i<n;i++)
		arr[i]=(char*)malloc((N+2)*sizeof(char));
	for (i=0;i<n;i++)
	{
		fflush(stdin);
		fgets(arr[i],N,stdin);
	}
	for (i=0;i<n;i++)
	{
		change(arr[i]);
		printf("\n");
	}
	return 0;
}