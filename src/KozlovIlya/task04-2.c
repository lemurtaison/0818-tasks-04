#include <stdio.h>
#include <stdlib.h>
#include <clocale>
#include <time.h>
#include <string.h>
#define count 10
#define len 20
int main()
{
	setlocale(LC_ALL, "Russian");
	FILE *infile, *outfile;
	int m, l, i, t, m1, j, test[count];
	char str[100], word[count][len];
	infile = fopen("infile.txt", "rt");
	outfile = fopen("outfile.txt", "wt");
	srand(time(0));
	do
	{
		fgets(str, 100, infile);
		printf("\n");
		m = 0;
		l = 0;
		for (i = 0; i<strlen(str); i++)
		{
			if ((str[i] != ' ') && (str[i] != '\n'))
			{
				word[m][l] = str[i];
				l++;
			}
			else
			{
				word[m][l] = 0;
				m++;
				l = 0;
			}

		}
		m1 = -1;
		while (m1<m - 1)
		{
			t = rand() % m;
			l = 0;
			j = 0;
			while ((j <= m1) && (l == 0))
			{
				if (t == test[j])
					l = 1;
				j++;
			}
			if (l == 0)
			{
				printf("%s ", word[t]);
				fprintf(outfile, "%s ", word[t]);
				m1++;
				test[m1] = t;
			}
		}
		printf("\n");
		fprintf(outfile, "\n");
	} while (!feof(infile));
	fclose(infile);
	fclose(outfile);
	return 0;
}
