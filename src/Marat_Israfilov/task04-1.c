/*Программа, подсчитывающая самую длинную последовательность Коллатца*/
#include <stdio.h>
#define N 1000000

typedef unsigned long long ull;

int collatz_sequence(ull num)
{
	ull lenght = 0;
	
	while(num != 1)
	{
		if(num % 2 == 0)
		{
			num = num / 2;
		}
		else
		{
			num = 3 * num + 1;
		}
		lenght++;
	}
	return lenght;
}

int main()
{
	ull i, number;
	int tmp, lenght = 0;

	for(i = 2; i < N; i++)
	{
		tmp = collatz_sequence(i);

		if(tmp > lenght)
		{
			lenght = tmp;
			number = i;
		}
	}
	
	printf("Number:%llu Lenght:%d\n", number, lenght);
	return 0;
}
