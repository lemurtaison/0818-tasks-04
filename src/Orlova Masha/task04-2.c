#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define N 128
int main()
{
	int sum, len, i, r, count, index, sort[N];
	char str[N], mas[N][N];
	srand(time(0));
	FILE *fp1, *fp2;
	fp1 = fopen("text1.txt", "rt");
	fp2 = fopen("text2.txt", "wt");
	for (sum, len; (fgets(str, N, fp1)); sum=0, len=0)
	{
		for (i = 0; i < strlen(str); i++)
		{
			if ((str[i] == ' ') || (str[i] == '\n'))
			{
				mas[sum][len] = 0;
				len = 0;
				sum++;
			}
			else
			{
			    mas[sum][len] = str[i];
				len++;
   		    }
		}
		for (count = 0; sum > count; len = 0)
		{
			r=rand() % sum;
			for (index = 0; (count >= index) && (len == 0); index++)
			{
				if (r == sort[index])
				len = 1;
			}
			if (len == 0)
			{
				fprintf(fp2, "%s ", mas[r]);
				count++;
         sort[count] = r;
			}
		}
		fprintf(fp2,"\n");
	}
	fclose(fp1);
	fclose(fp2);
	return 0;
}