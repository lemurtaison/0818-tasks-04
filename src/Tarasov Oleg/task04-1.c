#include <stdio.h>
#include <locale.h>
#include <stdlib.h>

#define MIN_VALUE 2
#define MAX_VALUE 1000000

int GetKollatsSeqLength(unsigned long long n)
{
	unsigned long long save = n;
	int num = 0;
	while (true) {
		num++;
		if (n == 1) break;
		if (n % 2 == 0)
			n /= 2;
		else {
			unsigned long long temp = n;
			n = n * 3 + 1;
		}
	}
	return num;
}


int main()
{
	setlocale(LC_ALL, "Russian");

	int i = MIN_VALUE, maxseq = 0, maxval = 0;
	while (i <= MAX_VALUE) {
		//printf("%d\n", i);
		int temp = GetKollatsSeqLength(i);
		if (temp > maxseq) {
			maxseq = temp;
			maxval = i;
		}
		i++;
	}
	printf("������������ ����� ������������������ ��������: %d (��� ����� %d).\n", maxseq, maxval);
	return 0;
}