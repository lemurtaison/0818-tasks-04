#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#define MIN 2
#define MAX 1000000
int main() {
	setlocale(LC_ALL, "Russian");
	int max = 0;
	int max_num = -1;
	for (int i = MIN; i <= MAX; i++) {
		int counter = 0;
		long long num = i;
		while (num != 1) {
			if (num % 2 == 0)
				num /= 2;
			else
				num = num * 3 + 1;
			counter++;
		}
		if (counter > max) {
			max = counter;
			max_num = i;
		}
	}
	printf("MAX: %i\n", max_num);
	return 0;
}
